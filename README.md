# Mini Project2 Cargo Lambda

## Live Demo
> [Live Demo](https://3rtlat0w35.execute-api.us-east-1.amazonaws.com/default/ids721-week2?number1=100&number2=5) Could be customized with your own numbers by changing the query string after the `number1` ans `number2` parameters.

## Functionality
Given two integers, the function calculates their production.

## Install Cargo Lambda
Follow the official instruction to install Cargo on Windows using rustup.

## Create a new Cargo Lambda project

The new subcommand will help you create a new project with a default template. When it's done, change into the new directory:
```sh
cargo lambda new ids721-week2 \
&& cd ids721-week2
```

## Serve the function locally for testing
Run the Lambda emulator built in with the watch subcommand:

```sh
cargo lambda watch
```

## Generating AWS User Credentials
At this point, you are probably champing at the bit to deploy your Rust function into the AWS Lambda service. In order to do that, we will need to generate some static AWS user credentials, so that we can publish the Lambda function from our local development system.
1. Open the AWS IAM web management console
2. Create a new IAM user
3. Choose Attach Policies Directly option
4. Search for the AWSLambda_FullAccess policy and check it
5. Finish the user creation wizard, and open the user details
6. Open the Security Credentials tab
7. Under Access Keys, select the Create Access Key button
8. Choose the Other option, and skip the Description field
9. Copy the Access Key and Secret Access Key values


## Deploy the function to AWS Lambda
```sh
cargo lambda build --release # release build first
cargo lambda deploy
```

**Note:** grant the user the necessary permissions to create the role.


## Add AWS API Gateway trigger to the function
1. Open the AWS Lambda web management console
2. Click on the function you want to add the trigger to
3. Click on the Add trigger button
4. Choose the API Gateway option
5. Choose the Create a new API option
6. Choose either the REST API option or the HTTP API option(I have tried both and they worked well)
7. Click on the Add button
8. Access the function using the URL provided in the API Gateway trigger details
![My API Gateway](./APIgateway.png)

## Note
While configuring the HTTP API Gateway, I noticed that there're two versions of payload format processing. While it didn't show any impact on my current project, it affected the functionality when I was trying to manipulate a list in my application, as only 2.0 version could be compatible for list manipulation.
