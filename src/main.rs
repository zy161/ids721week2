use lambda_http::{lambda_runtime::{self, Error, Context}, Request, RequestExt, IntoResponse, http::StatusCode, service_fn, run};
use serde::{Deserialize, Serialize};
use serde_json::json;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};


#[derive(Deserialize)]
struct QueryParameters {
    number1: i32,
    number2: i32,
}

async fn function_handler(event: Request) -> Result<impl IntoResponse, Error> {
    // Extract query string parameters
    let query_params = event.query_string_parameters();
    //let number1 = query_params.get("number1").and_then(|n| n.parse::<i32>().ok()).unwrap_or(0);
    //let number2 = query_params.get("number2").and_then(|n| n.parse::<i32>().ok()).unwrap_or(0);
    let number1 = query_params.first("number1").map_or(0, |n| n.parse::<i32>().unwrap_or(0));
    let number2 = query_params.first("number2").map_or(0, |n| n.parse::<i32>().unwrap_or(0));
    // Calculate the product
    let product = number1 * number2;

    // Create a response
    Ok(lambda_http::Response::builder()
        .status(StatusCode::OK)
        .body(json!({ "product": product }).to_string())
        .expect("Failed to render response"))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).from_env_lossy())
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
